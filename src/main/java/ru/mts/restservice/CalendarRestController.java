package ru.mts.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mts.data.Date;
import ru.mts.data.Time;

@RestController
public class CalendarRestController {

    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/date")
    public Date date() {
        return new Date(counter.incrementAndGet());
    }

    @GetMapping("/time")
    public Time time() {
        return new Time(counter.incrementAndGet());
    }
}