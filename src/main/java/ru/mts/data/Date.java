package ru.mts.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Date {

    public Date(long id) {
        this.id = id;
    }

    final long id;
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public long getId() {
        return id;
    }

    public String getDate() {
        LocalDate date = LocalDate.now();
        return date.format(formatter);
    }
}
