package ru.mts.data;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Time {

    public Time(long id) {
        this.id = id;
    }

    final long id;
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public long getId() {
        return id;
    }

    public String getTime() {
        LocalTime time = LocalTime.now();
        return time.format(formatter);
    }
}
